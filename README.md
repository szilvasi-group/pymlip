# pymlip

This python package is a collection of scripts to use for training machine learning interatomic potentials, or mlips, in python.

This package is currently interfaced to the following codes
- Nequip
- Allegro

# Learning Rate Module

In the `pymlip/lrlambda.py` file, there are pre-defined learning rate schedules which are compatible with Nequip directly.  See the `examples` folder to see how to input these learning rates into the nequip.yaml files.


