import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()


setuptools.setup(
        name="pymlip",
        version="0.1.1",
        scripts=['bin/', 'bin/'],
        author="Tristan Maxson",
        author_email="tgmaxson@gmail.com",
        description="Package of scripts for MLIP training, validation, and deployment developed while in Szilvasi Group",
        long_description=long_description,
        long_description_content_type="text/markdown",
        project_urls={
            'Gitlab': 'https://gitlab.com/tgmaxson/pymlip'
            },
        python_requires=">=3",
        install_requires=[
            'numpy',
            'scipy',
            'icecream',
        ],
        packages=setuptools.find_packages(),
        classifiers=[
            "Intended Audience :: Science/Research",
            "Programming Language :: Python :: 3",
            "Topic :: Scientific/Engineering"]
        )


